# 目的

このスクリプトは、Raspberry PIにWebCameraを繋ぎ、
カメラが動体を検知したら自動的に暗号化zipとして
動画をDropbox上にアップロードする。
自分が自宅にいるときにはアップロードは不要なため、
スマートフォンがPing応答を返すときには
アップロードは行わない。

# Setup

## Install zip
    sudo apt-get install zip

## Download dropbox_uploader.sh and setup.
    https://github.com/andreafabrizi/Dropbox-Uploader
  
    Note: 'sudo -u motion dropbox_uploader.sh' for setup.

## Install motion and setup
   
    sudo apt-get install motion

    edit /etc/motion/
   
     ffmpeg_output_movies on
   
     on_movie_end "/home/pi/motion/on_movie_end.sh %f"

## Setup mobile phone

	Setting static IP address.(ex.192.168.1.10)
	
	edit(or make) /home/pi/motion/.config
	
	 PING_TARGET="192.168.1.10"

## Setup password file for zip

	edit /home/pi/motion/.config

	 PSWD="YOUR PASSWORD FOR ZIP"

## Setup crontab
   	sudo -u root crontab -e
   	 */10 *    * * *   /home/pi/motion/stop_service.sh 2>>  /home/pi/motion/cron.log
	 */5 *    * * *   /home/pi/motion/start_service.sh 2>> /home/pi/motion/cron.log

# setup for speak

	 add motion in audio group for speak
	 
	 sudo gpasswd -a motion audio

	 setup dcmTalk

	 setup gspeech

# Tips
	 If can't open video 0, you should edit /etc/rc.local

	 chmod 666 /dev/video0

         or sudo gpasswd -a motion video (NOT test yet)

　　　　　　　　If can't speak, you should add motion to autio group

         sudo gpasswd -a motion audio


## Daemon

 * /etc/motion/motion.conf

    daemon on
   
 * /etc/default/motion
   
    start_motion_daemon=yes

 * user is 'motion'

## Use ramdisk for tmp movie.

 * /etc/fstab
   
    tmpfs           /tmp/motion/            tmpfs   defaults,size=32m 0       0