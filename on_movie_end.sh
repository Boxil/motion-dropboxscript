#!/bin/sh

MOTION_DIR=/home/pi/motion/
AquesTalkPi=/home/pi/aquestalkpi/AquesTalkPi
dcmTalk=/home/pi/dcmTalk/dcmTalk.rb
GSPEECH_REC="/home/pi/gspeech-rec/speech-rec.sh"
#check plughw aplay -l
APLAY="aplay -D plughw:1,0"
USE_TALK=1

# external config file
# PSWD=hoge
# PING_TARGET="192.168.1.20"
. ${MOTION_DIR}.config

FN=$1
AVI=${FN%.*}".avi"
ZIPFILE=${FN%.*}".zip"
DROPBOXFILE=${ZIPFILE##*/}

if [ $# -ne 1 ]; then
    echo "invalid argument: on_movie_end.sh [%f]" 1>&2
    exit 1
fi

# exit script if do have a response
# speak 'Welcome back' when I came home.
# (Should enable stop delay option on in service stop script for speak)
ping ${PING_TARGET} -c 2 >> /dev/null
if [ $? -eq 0 ]; then
    if [ $USE_TALK -eq 1 ];then
	if [ ! -e $WELCOME_DONE ]; then
	    $APLAY ${MOTION_DIR}voice/okaeri.wav
	    date >> ${MOTION_DIR}welcome_date.log
	    touch $WELCOME_DONE
	    chmod 666 $WELCOME_DONE
	    $APLAY ${MOTION_DIR}voice/kyouhadou.wav
	    $GSPEECH_REC > ${MOTION_DIR}output.dat
	    ret=$?
	    $dcmTalk ${MOTION_DIR}output.dat
	    rm ${MOTION_DIR}output.dat
	    if [ ${ret} -eq 0 ];then 
		$dcmTalk "で登録します。"
	    fi
	fi
    fi
    rm -f ${AVI}
   exit 2
fi
       

zip -e -P ${PSWD} -1 -D ${FN%.*}".zip" ${AVI}

${MOTION_DIR}dropbox_uploader.sh upload $ZIPFILE $DROPBOXFILE
ret=$?
# delete files when succed at uploading to dropbox
if [ $ret -eq 0 ];then
    rm -f ${AVI} &
    rm -f $ZIPFILE &
    echo "$DROPBOXFILE" >> ${MOTION_DIR}log.txt
    if [ $USE_TALK -eq 1 ];then
	$APLAY ${MOTION_DIR}voice/satuei.wav
    fi
fi

