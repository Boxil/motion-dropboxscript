MOTION_DIR=/home/pi/motion/

. ${MOTION_DIR}.config

count=`ps -A | grep motion | grep -v grep | wc -l`
if [ $count -ne 0 ]; then
    exit 1
fi

# start service if do NOT have a response
/bin/ping ${PING_TARGET} -c 3 >> /dev/null
if [ $? -ne 0 ]; then
    /usr/sbin/service motion start
    exit 2
fi
