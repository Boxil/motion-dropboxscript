#!/bin/sh

MOTION_DIR=/home/pi/motion/

. ${MOTION_DIR}.config

USE_DELAY=1
DELAY_TIME=5m

# exit service if have a response
/bin/ping ${PING_TARGET} -c 3 >> /dev/null
if [ $? -eq 0 ]; then
    if [ ${USE_DELAY} -eq 1 ]; then
	/bin/sleep $DELAY_TIME
    fi
    /usr/sbin/service motion stop
    rm $WELCOME_DONE
    exit 2
fi
